import Foundation
import Combine

protocol CountriesRepository {
  func fetchAllCountries() -> AnyPublisher<[Country], Never>
}

class CountriesRepositoryImpl: CountriesRepository {
  
  func fetchAllCountries() -> AnyPublisher<[Country], Never> {
    
    let url = URL(string: "https://restcountries.com/v3.1/all")
    
    guard let url = url else {
      return Just([]).eraseToAnyPublisher()
    }
    
    return URLSession.shared.dataTaskPublisher(for: url)
      .map(\.data)
      .decode(type: [Country].self, decoder: JSONDecoder())
      .map { result in
        result
      }
      .replaceError(with: [])
      .eraseToAnyPublisher()
  }
}

enum countriesError: Error {
  case noData
  case invalidUrl
}
