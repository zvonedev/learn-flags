//
//  HomeViewModel.swift
//  CustomStylesSwiftUI
//
//  Created by Zvonimir Pandža on 28.03.2022..
//

import Foundation

class HomeViewModel: ObservableObject {
  @Published var countries: [Country] = []
  
  
  init() {
    loadData()
  }
  
  private func loadData() {
    URLSession.shared.dataTask(with: URL(string: "https://restcountries.com/v3.1/all")!) { data, response, error in
      if let error = error {
        print("Error while loading data: \(error.localizedDescription)")
        return
      }
      
      guard let data = data else {
        print("No data!")
        return
      }
      
      let countries = try! JSONDecoder().decode([Country].self, from: data)
    
      DispatchQueue.main.async {
        self.countries = countries
      }
    }
    .resume()
  }
}
