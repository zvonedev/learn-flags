import Foundation

// MARK: - Country
struct Country: Codable, Identifiable, Equatable {
  static func == (lhs: Country, rhs: Country) -> Bool {
    lhs.name.common == rhs.name.common
  }
  
  let id = UUID().uuidString
  let name: Name
  let flags: Flags
}

// MARK: - Name
struct Name: Codable {
  let common, official: String
  let nativeName: NativeName?
}

// MARK: - NativeName
struct NativeName: Codable {
  let spa: SPA?
}

// MARK: - SPA
struct SPA: Codable {
  let official, common: String
}

struct Flags: Codable {
    let png: String
    let svg: String
}
