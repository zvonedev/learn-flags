import SwiftUI

struct HomeView: View {
  
  @StateObject var viewModel: HomeViewModel = HomeViewModel()
  
  var body: some View {
    List(viewModel.countries) { country in
      HStack {
        Text(country.name.common)
        Spacer()
        AsyncImage(
          url: URL(string: country.flags.png),
          content: {
            $0.resizable()
              .aspectRatio(contentMode: .fit)
              .frame(maxWidth: 100, maxHeight: 60)
          },
          placeholder: {
            ProgressView()
          }
        )
      }
      .frame(height: 120)
    }
  }
}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
