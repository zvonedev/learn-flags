import SwiftUI

struct QuizContainerView: View {
  
  @State private var tempBool = false
  
  @ObservedObject private var viewModel: QuizViewModel
  
  init(viewModel: QuizViewModel) {
    self.viewModel = viewModel
  }
  
  var body: some View {
    if viewModel.isQuizActive {
      QuizActiveView(viewModel: viewModel)
    } else {
      VStack {
        Button("Start Quiz!") {
          print("Start!")
          viewModel.startQuiz()
        }
        .positiveButton()
      }
    }
  }
}

//struct QuizContainerView_Previews: PreviewProvider {
//    static var previews: some View {
//      QuizContainerView(viewModel: QuizViewModel(fetchAllCountriesUseCase: <#T##FetchAllCountriesUseCase#>))
//    }
//}
