import Foundation
import Combine

class QuizViewModel: ObservableObject {
  
  @Published var isQuizActive: Bool = false
  @Published var scoreCounter: Int = 0
  @Published var currentCountryName: String = "Uruguay"
  @Published var currentCountryFlag: String = ""
  @Published var currentAnswers: [String] = []
  
  private var countries: [Country] = []
  private var cancellable = Set<AnyCancellable>()
  
  let fetchAllCountriesUseCase: FetchAllCountriesUseCase
  
  init(fetchAllCountriesUseCase: FetchAllCountriesUseCase) {
    self.fetchAllCountriesUseCase = fetchAllCountriesUseCase
  }
  
  func quizStarted() {
    fetchAllCountriesUseCase.execute()
      .receive(on: DispatchQueue.main, options: nil)
      .sink { countries in
        self.countries = countries
        self.prepareQuiz()
      }
      .store(in: &cancellable)
  }
  
  func endQuiz() {
    isQuizActive = false
    scoreCounter = 0
    currentAnswers = []
    currentCountryName = ""
  }
  
  func startQuiz() {
    isQuizActive = true
  }
  
  func answerSelected(_ answer: String) {
    if currentCountryName == answer {
      scoreCounter += 1
    } else {
      scoreCounter = 0
    }
    prepareQuiz()
  }
  
  func onAppear() {
    if currentAnswers.isEmpty {
      quizStarted()
    }
  }
  
  func prepareQuiz() {
    currentAnswers = []
    guard let currentCountry = countries.randomElement() else { return }

    var tempAnswers = [String]()
    tempAnswers.append(currentCountry.name.common)
    
    var whileFlag = true
    
    while whileFlag {
      guard let tempCountry = countries.randomElement() else { return }
      let tempCountryName = tempCountry.name.common
      
      if !tempAnswers.contains(tempCountryName) {
        tempAnswers.append(tempCountryName)
        if tempAnswers.count == 4 {
          whileFlag = false
        }
      }
    }
    
    DispatchQueue.main.async {
      self.currentCountryName = currentCountry.name.common
      self.currentCountryFlag = currentCountry.flags.png
      self.currentAnswers.append(contentsOf: tempAnswers.shuffled())
    }
  }
}
