import SwiftUI

struct QuizActiveView: View {
  
  @StateObject var viewModel: QuizViewModel
  
  var items: [GridItem] = Array(repeating: .init(.adaptive(minimum: 100, maximum: 400)), count: 2)
  
  var body: some View {
    VStack {
      
      HStack {
        AsyncImage(
          url: URL(string: viewModel.currentCountryFlag),
          content: {
            $0.resizable()
              .aspectRatio(contentMode: .fit)
              .frame(maxWidth: 100, maxHeight: 60)
          },
          placeholder: {
            ProgressView()
          }
        )
        Spacer()
        Text("Score: \(viewModel.scoreCounter)")
              .background(RoundedRectangle(cornerRadius: 8).fill(Color.gray.opacity(0.5)))
      }
      LazyVGrid(columns: items, spacing: 50) {
        ForEach(viewModel.currentAnswers, id: \.self) { answer in
          Text(answer)
            .onTapGesture {
              viewModel.answerSelected(answer)
            }
        }
      }
      Button("End Quiz!") {
        print("End!")
        viewModel.endQuiz()
      }
      .onAppear(perform: {
        viewModel.onAppear()
      })
      .negativeButton()
    }
  }
}

//struct QuizActiveView_Previews: PreviewProvider {
//  static var previews: some View {
//    QuizActiveView(viewModel: QuizViewModel())
//  }
//}
