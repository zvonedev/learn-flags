import SwiftUI

struct PositiveButtonConfiguration: ViewModifier {
  func body(content: Content) -> some View {
    content
      .font(.title)
      .foregroundColor(.white)
      .padding()
      .background(Color.blue)
      .clipShape(RoundedRectangle(cornerRadius: 20))
  }
}

struct NegativeButtonConfiguration: ViewModifier {
  func body(content: Content) -> some View {
    content
      .font(.title)
      .foregroundColor(.white)
      .padding()
      .background(Color.red)
      .clipShape(RoundedRectangle(cornerRadius: 20))
  }
}
