import Foundation

protocol RepositoryProvider {
  func makeCountriesRepository() -> CountriesRepository
}

class RepositoryProviderImpl: RepositoryProvider {
  func makeCountriesRepository() -> CountriesRepository {
    return CountriesRepositoryImpl()
  }
}
