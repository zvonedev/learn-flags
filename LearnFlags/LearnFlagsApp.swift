import SwiftUI

@main
struct LearnFlagsApp: App {
  var body: some Scene {
    WindowGroup {
      let repositoryProvider = RepositoryProviderImpl()
      let useCaseProvider = UseCaseProviderImpl(repositoryProvider: repositoryProvider)
      let viewBuilder = ViewBuilder(useCaseProvider: useCaseProvider)
      
      ContentView(viewBuilder: viewBuilder)
    }
  }
}
