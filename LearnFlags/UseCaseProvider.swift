import Foundation

protocol UseCaseProvider {
  func makeFetchAllCountriesUseCase() -> FetchAllCountriesUseCase
}

class UseCaseProviderImpl: UseCaseProvider {
  
  let repositoryProvider: RepositoryProvider
  
  init(repositoryProvider: RepositoryProvider) {
    self.repositoryProvider = repositoryProvider
  }
  
  func makeFetchAllCountriesUseCase() -> FetchAllCountriesUseCase {
    FetchAllCountriesUseCaseImpl(countriesRepository: repositoryProvider.makeCountriesRepository())
  }
}
