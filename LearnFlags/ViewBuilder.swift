import Foundation
import SwiftUI

class ViewBuilder {
  
  private let useCaseProvider: UseCaseProvider
  
  init(useCaseProvider: UseCaseProvider) {
    self.useCaseProvider = useCaseProvider
  }
  public lazy var quizScene: QuizContainerView = {
    let viewModel = QuizViewModel(fetchAllCountriesUseCase: useCaseProvider.makeFetchAllCountriesUseCase())
    
    return QuizContainerView(viewModel: viewModel)
  }()
}
