import SwiftUI

struct ContentView: View {
  
  @State var selectedTab: Int = 0
  
  let viewBuilder: ViewBuilder
  
  var body: some View {
    NavigationView {
      TabView(selection: $selectedTab) {
        HomeView()
          .tabItem {
            Label("Countries", systemImage: "person.fill")
          }
          .tag(0)
        viewBuilder.quizScene
          .tabItem {
            Label("Quiz", systemImage: "pencil")
          }
          .tag(1)
      }
      .navigationTitle(navigationTitleString)
    }
  }
}

extension ContentView {
  var navigationTitleString: String {
    switch selectedTab {
    case 0:
      return "Home"
    case 1:
      return "Quiz"
    default:
      return "Default"
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    let repositoryProvider = RepositoryProviderImpl()
    let useCaseProvider = UseCaseProviderImpl(repositoryProvider: repositoryProvider)
    let viewBuilder = ViewBuilder(useCaseProvider: useCaseProvider)
    
    ContentView(viewBuilder: viewBuilder)
  }
}
