import SwiftUI

extension View {
  func positiveButton() -> some View {
    modifier(PositiveButtonConfiguration())
  }
  
  func negativeButton() -> some View {
    modifier(NegativeButtonConfiguration())
  }
}

