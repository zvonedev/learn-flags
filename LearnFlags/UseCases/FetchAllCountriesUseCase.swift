import Foundation
import Combine

protocol FetchAllCountriesUseCase {
  func execute() -> AnyPublisher<[Country], Never>
}

class FetchAllCountriesUseCaseImpl: FetchAllCountriesUseCase {
  
  private let countriesRepository: CountriesRepository
  
  init(countriesRepository: CountriesRepository) {
    self.countriesRepository = countriesRepository
  }
  
  func execute() -> AnyPublisher<[Country], Never> {
    return countriesRepository.fetchAllCountries()
  }
}
